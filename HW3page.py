'''!@file               HW3page.py
    @brief              Creates the main documentation page for HW 3.
    @details            This file serves the sole purpose of generating html
                        documentation for HW 3.

    @page pagehw3       Homework 3 Deliverables

    @section sec_introhw2 Introduction
                        ME 305 HW 3 shows the ball balancer system simulation
                        that will be used in the final project. This page includes
                        the full MATLAB script used to simulate this system, 
                        along with an image of the corresponding Simulink
                        block diagram.
                        
    @section sec_MATLAB3 MATLAB Script for Homework 3
                        Shown below is the MATLAB script used to simulate this
                        ball-balancer system.
                                                    
                        \htmlonly
                        <a href="https://ibb.co/7KNVS71"><img src="https://i.ibb.co/jDZG87r/HW3-MATLAB-full-1.png" alt="HW3-MATLAB-full-1" border="0"></a>
                        <a href="https://ibb.co/NSkjkxt"><img src="https://i.ibb.co/jRxMxhb/HW3-MATLAB-full-2.png" alt="HW3-MATLAB-full-2" border="0"></a>
                        <a href="https://ibb.co/x6WRfts"><img src="https://i.ibb.co/kXjW9sy/HW3-MATLAB-full-3.png" alt="HW3-MATLAB-full-3" border="0"></a>
                        <a href="https://ibb.co/DMvbYNc"><img src="https://i.ibb.co/QHTckZS/HW3-MATLAB-full-4.png" alt="HW3-MATLAB-full-4" border="0"></a>
                        <a href="https://ibb.co/HG23nKk"><img src="https://i.ibb.co/nQ0tzw5/HW3-MATLAB-full-5.png" alt="HW3-MATLAB-full-5" border="0"></a>
                        <a href="https://ibb.co/9h4LYjB"><img src="https://i.ibb.co/Jqp4kP8/HW3-MATLAB-full-6.png" alt="HW3-MATLAB-full-6" border="0"></a>
                        <a href="https://ibb.co/z5SQbfF"><img src="https://i.ibb.co/Ctnmv06/HW3-MATLAB-full-7.png" alt="HW3-MATLAB-full-7" border="0"></a>
                        <a href="https://ibb.co/k3X8TTj"><img src="https://i.ibb.co/ZHx8rr5/HW3-MATLAB-full-8.png" alt="HW3-MATLAB-full-8" border="0"></a>
                        \endhtmlonly
    


    @author             Jake Lesher and Daniel Xu
    
    @copyright          License Info

    @date               February 21, 2022
'''