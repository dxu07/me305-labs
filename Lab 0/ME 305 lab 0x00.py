# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 12:35:10 2022
@file ME 305 lab 0x00.py
@author: jakel
"""


def fib(idx):                                                                   # Here, the fib() function is defined. This approach is a "bottom-up" 
    f = [0]*(idx+1)                                                             # approach to calculating the Fibonacci number requested.
    for i in range(idx+1):
        if i < 2:
            f[0] = 0
            f[1] = 1
        else:
            f[i]=f[i-2]+f[i-1]
    return f[idx]


              
if __name__ == '__main__':       
    def script():                                                               # This script() function allows me to run the rest of this script as
                                                                                # a function. This is key for being able to restart the program.
        
                                                                                # This if statement will make sure that all of the following prompts
        idx = input('Please enter a Fibonacci index number: ')                  # only show when this file is run as a main program. When imported,
                                                                                # the functionality of fib(idx) will not have any messages/UI.

        while idx.isdigit() == False:                                           # This while loop will check to make sure that the user input
            idx = input('ERROR. Try again. '                                    # is an integer. Strings, negative numbers, and any other invalid
                        'Please enter a valid Fibonacci index number. '         # input will result in the following ERROR message and a chance 
                        'This number should be whole and greater than zero. ')  # to reinput the index number.

        idx = int(idx)                                                          # This line converts the user's string input into an integer for use 
        print('Fibonacci number at '                                            # in the fib(idx) function. It then prints the solution.
              'index {:} is {:}.'.format(idx, fib(idx)))
        
        restart = input("Would you like to restart? (y/n) ")
        if restart == "yes" or restart == "y":
            script()
        if restart == "no" or restart == "n":
            print('Script terminating. Bye!')
            
    
    script()                                                                    # This line runs the function script(), which is essentially every 
                                                                                # prompt and message coming through the console.