'''!
    @file       BlinkingLED.py
    
    @brief      Cycles the Nucleo board through 3 LED blinking
                patterns, switching patterns at the press of a button.
    
    @details    On startup, the program will be in an idle state until the 
                button is pushed for the first time. A press of the blue 
                button will trigger the LED to flash in a square wave pattern. 
                Subsequent presses will switch to a sine wave and then to a 
                sawtooth wave. Another button press will return the system to 
                the square wave pattern, starting the cycle over.
                
                A video demonstration of the functionality of this program
                can be found at this link: https://vimeo.com/668040642
                
                The source code can be found at my repository under "Lab 1":
                https://bitbucket.org/jakelesher/me-305-labs
    
    @image html https://i.ibb.co/XXT2hdq/lab1diagrams.jpg "LED Blinking State Transition Diagram" width=1200px
    
    @author     Jake Lesher
    @author     Daniel Xu
    @date       01/13/2022
'''
import time
import math
import pyb

def onButtonPressFCN(IRQ_src):
    '''!            Intermediate function to identify when the blue button 
                    is pressed.
        @details    This function sets ButtonIsPressed to true. It is triggered
                    by an interrupt.
        @param      IRQ_src can be ignored. This function is only used within 
                    ButtonInt.
        @return     Nothing is returned. Instead, ButtonIsPressed will be set 
                    to true.
    '''
    global ButtonIsPressed
    ButtonIsPressed = True
   
def SquareWave(TimeSinceStart):
    '''!            A function of time that returns brightness in a square
                    wave pattern.
        @details    This function uses the modulus to consider 1000 ms chunks
                    of TimeSinceStart. Looking at each second, the brightness
                    will be set to 0 or 100 depending on how much time remains.
        @param      TimeSinceStart is an input of time based on when the button
                    is pressed.It has units of ms.
        @return     This function returns a brightness value, either 0 or 100.
            
    '''
    trigger = TimeSinceStart % 1000
    if trigger <= 500:
        brightness = 100
    else:
        brightness = 0
    return brightness

def SineWave(TimeSinceStart):
    '''!            A function of time that returns brightness in a sine wave
                    pattern.
        @details    This function uses the math library to have the brightness
                    output follow a sine wave with an amplitude of 50, centered
                    at 50. The amplitude of the sine wave has units of percent.
        @param      TimeSinceStart is an input of time based on when the button
                    is pressed. It has units of ms.
        @return     This function returns a brightness value between 0 and 100.
            
    '''
    brightness = 50*math.sin(TimeSinceStart*2*3.14159/10000)+50
    return brightness

def SawWave(TimeSinceStart):
    '''!            A function of time that returns brightness in a sawtooth
                    wave pattern.
        @details    This function uses the modulus to have the brightness
                    increase within each block of 1000 ms. Since the modulus 
                    is essentially the remainder, the brightness will increase
                    until 1000 ms and then reset and loop.
        @param      TimeSinceStart is an input of time based on when the button
                    is pressed. It has units of ms.
        @return     This function returns a brightness value between 0 and 100.
            
    '''
    brightness = (TimeSinceStart % 1000)/10
    return brightness

if __name__ == '__main__':
    
    #Initializing pins
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    
    #Setting up variables
    
    ##  @brief      The variable, 'state', is used to cycle between the three
    #               blinking patterns, as well as the idle state.
    #   @details    The initial value for 'state' is 0, which is the idle state
    #               in which nothing happens. With each button press, the value
    #               of 'state' will increase until it is reset to 1 after 3.
    state = 0
    
    ##  @brief      The variable, 'brightness', is an integer between 0 and 100
    #               meant to be used as a percentage when setting the LED PWM.
    #   @details    This variable is updated very frequently by whatever 
    #               function the active state is using.
    brightness = 0
    
    ##  @brief      The boolean, 'ButtonIsPressed', is meant to trigger the if
    #               statements that change the 'state' variable.
    #   @details    This boolean is changed to true by the interrupt function 
    #               called onButtonPressFCN().
    ButtonIsPressed = False
    
    #Setting up button interrupt
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, 
                           pull=pyb.Pin.PULL_NONE, callback = onButtonPressFCN)        
   
    #Defining timers for PWM
    tim2 = pyb.Timer(2, freq = 20000)
    t2ch1 = tim2.channel(1,pyb.Timer.PWM, pin = pinA5)
    
    #Displaying welcome message.
    print('Welcome to LED Blinker. The LED is currently idle. Press the blue'
          ' button on the board to begin cycling through blink patterns.')
    
    while True:
        # With this loop we want to look for a keyboard interrupt.
        # It will try to run the code until something happens and then except.
        try:
            current_time = time.ticks_ms()
            if state == 0:
                if ButtonIsPressed:
                    start_time = time.ticks_ms()
                    print('Square wave pattern selected.')
                    ButtonIsPressed = False
                    state = 1
                    
            elif state == 1:
                WaveformTime = time.ticks_diff(current_time, start_time)
                brightness = SquareWave(WaveformTime)
                t2ch1.pulse_width_percent(brightness)
                if ButtonIsPressed:
                    start_time = time.ticks_ms()
                    print('Sine wave pattern selected.')
                    ButtonIsPressed = False
                    state = 2
                    
            elif state == 2:
                WaveformTime = time.ticks_diff(current_time, start_time)
                brightness = SineWave(WaveformTime)
                t2ch1.pulse_width_percent(brightness)
                if ButtonIsPressed:
                    start_time = time.ticks_ms()
                    print('Sawtooth wave pattern selected.')
                    ButtonIsPressed = False
                    state = 3
                
            elif state == 3:
                WaveformTime = time.ticks_diff(current_time, start_time)
                brightness = SawWave(WaveformTime)
                t2ch1.pulse_width_percent(brightness)
                if ButtonIsPressed:
                    start_time = time.ticks_ms()
                    print('Square wave pattern selected.')
                    ButtonIsPressed = False
                    state = 1
                    
                    
        except KeyboardInterrupt:
            # A KeyboardInterrupt is ctrl+C in the terminal.
            break
        
    print("Program Terminating")