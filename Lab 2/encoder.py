'''!
    @file       encoder.py
    
    @brief      Encoder driver to be used with one or two quadrature encoders 
                connected to the Nucelo board.
    
    @details    This encoder driver functions to interact with the quadrature 
                encoder connected to the Nucleo board. This encoder driver is 
                capable of working with different encoders, as long as the 
                correct pins and timer is specified. This encoder driver 
                interacts with taskEncoder.py to frequently call upon the 
                update function to determine the position of the encoder. The 
                position, change in position(delta), and the zeroing of the 
                encoder are done in the code. 
    
                The source code can be found at my repository under "Lab 2":
                https://bitbucket.org/jakelesher/me-305-labs
                
    @author     Jake Lesher
    @author     Daniel Xu
    @date       01/31/2022
    
'''
import pyb

class Encoder:
    '''!@brief      Encoder driver class to interact with the Nucleo and the
                    hardware.
        @details    The encoder driver controls majority of the hardware 
                    interaction. It initializes the encoder and sets up a 
                    system to update the encoder position when called 
                    frequently. 
                    
    '''
    def __init__(self, pinA, pinB, timNum):
        '''!@brief      A function that initializes the encoders.
            @details    This function initializes the encoder attached to the 
                        Nucleo. In the initialization state, a timer object and
                        two channel objects are created and associated with the 
                        encoder. Additionally, the position of the encoder is 
                        zeroed.
            @param      pinA is an input that defines the pin where the first 
                        detector of the encoder is connected to. pinA is 
                        initialized in taskEncoder.py
            @param      pinB is an input that defines the pin where second 
                        detector of the encoder is connected to. pinB is 
                        initialized in taskEncoder.py
            @param      timNum specifies the specific timer that is associated 
                        with the encoder connection. 
            
        '''
        # Creation of Timer Object
        self.tim = pyb.Timer(timNum, prescaler=0, period=0xFFFF)
        
        # Creation of Channel Objects
        self.tim.channel(1, pyb.Timer.ENC_AB, pin=pinA)
        self.tim.channel(2, pyb.Timer.ENC_AB, pin=pinB)
        
        # Zeroing the encoder
        self.position = 0
        self.totalposition = 0

        
    def update(self):
        '''!@brief      A function that updates the position of the encoder.
            @details    This function updates the position of the encoder 
                        without overflow or rollback. It does this by 
                        determining the change in position with every update 
                        and adding up the changes to determine the actual 
                        position. This allows the position counter to go above 
                        0xFFFF and below 0.
            @return     self.totalposition which is the actual cummulitive 
                        position of the encoder.
            
        '''
        # Auto reload value
        AR = 2**16 #this is AR + 1, not actual AR
        
        # Calculating change in position between updates
        self.prevposition = self.position
        self.position = self.tim.counter()
        self.delta = self.position - self.prevposition
        
        # Accounting for overflow
        if self.delta > AR/2:
            self.delta -= AR
        elif self.delta < -AR/2:
            self.delta += AR
        
        # Updating Total Position
        self.totalposition += self.delta 
        
        # returns the total position
        return(self.totalposition)
        
    def get_position(self):
        '''!@brief      This function returns the most recent position of the 
                        encoder.
            @details    This function simply returns the total position 
                        calculated in the update section.
            @return     self.totalposition which is the actual cummulitive 
                        position of the encoder.
        
        '''
        return self.totalposition
    
    def zero(self):
        '''!@brief      This function zeroes the encoder.
            @details    This function resets the total position of the encoder 
                        by setting it to 0. 
                        
        '''
        self.position = 0
        self.totalposition = 0
        self.prevposition = 0
        self.delta = 0
        
        
    def get_delta(self):
        '''!@brief      This function returns the most recent change in position
                        of the encoder.
            @details    This function simply returns the delta position 
                        calculated in the update section.
            @return     self.delta position which is the change in position 
                        from the last update to the present update.
                        
        '''
        return self.delta
    