'''!
    @file       main.py
    
    @brief      The main script for Lab 2.
    
    @details    The purpose of this main script is to run the two tasks 
                responsible for the functionality of this encoder lab.
                The shares/queues that will be used in this lab are also 
                primarily defined in this script.
    
                A video demonstration of the functionality of this program
                can be found at this link: https://vimeo.com/
                
                The source code can be found at my repository:
                https://bitbucket.org/jakelesher/me-305-labs
                
    @author     Jake Lesher
    @author     Daniel Xu
    @date       01/31/2022
'''

import taskUser, taskEncoder, shares

##  @brief      The variable, zFlag, is a shared variable
#   @details    This shared variable is a boolean that is shared between 
#               taskUser and taskEncoder to determine whether or not the 
#               encoder object needs to be zeroed.
#  
zFlag = shares.Share(False)
##  @brief      The variable, Data, is a queue of shared data.
#   @details    This shared queue is the positional data of the encoder. 
#               It is updated in the taskEncoder and recorded in the taskUser.
#  

Data = shares.Queue()
##  @brief      The variable, Delta, is a queue of shared data.
#   @details    This shared queue is the delta data of the encoder. It is 
#               updated in the taskEncoder and recorded in the taskUser.
#  
Delta = shares.Queue()

if __name__ == '__main__':
    
    # taskList will be the list used to define the two tasks that will run
    # sequentially in 10 ms intervals.
    taskList = [taskEncoder.taskEncoderFcn('taskEncoder', 10_000, zFlag, Data, Delta),
                taskUser.taskUserFcn('taskUser', 10_000, zFlag, Data, Delta)]
    
    while True:
        
        # With this loop we want to look for a keyboard interrupt (Ctrl+C).
        # It will try to run the code until Ctrl+C happens and then break.
        try:
            for task in taskList:
                next(task)
            
        except KeyboardInterrupt:
            # A KeyboardInterrupt is ctrl+C in the terminal.
            break
        
    print("Program Terminating")