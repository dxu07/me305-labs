'''!
    @file       main.py
    
    @brief      The main script for running the tasks of Lab 3.
    
    @details    The purpose of this main script is to run the tasks 
                responsible for the functionality of lab 3.
                The shares/queues that will be used in this lab are also 
                primarily defined in this script.
                
                The source code can be found at my repository:
                https://bitbucket.org/jakelesher/me-305-labs
                
    @author     Jake Lesher
    @author     Daniel Xu
    @date       02/16/2022
'''

import taskUser, taskEncoder, taskMotor, shares

##  @brief      The variable, zFlag, is a shared variable
#   @details    This shared variable is a boolean that is shared between 
#               taskUser and taskEncoder to determine whether or not the 
#               encoder object needs to be zeroed.
#  
zFlag = shares.Share(False)

##  @brief      The variable, Data, is a queue of shared data.
#   @details    This shared queue is the positional data of the encoder, in 
#               radians. It is updated in taskEncoder and recorded in taskUser.
#  
Data = shares.Share()

##  @brief      The variable, Delta, is a queue of shared data.
#   @details    This shared queue is the delta data of the encoder in radians.
#               It is updated in taskEncoder and recorded in taskUser.
#  
Delta = shares.Share()

##  @brief      The variable, Velocity, is a queue of shared data.
#   @details    This shared queue is the current angular velocity in rad/s.
#               It is updated in taskEncoder and recorded in taskUser.
#
Velocity = shares.Share()

##  @brief      The variable, Duty1, is a queue of shared data.
#   @details    This shared queue is the duty cycle for motor 1 as requested
#               in taskUser. The queue is then read in taskMotor, where it is
#               sent to the motor driver.
#
Duty1 = shares.Share()

##  @brief      The variable, Duty2, is a queue of shared data.
#   @details    This shared queue is the current angular velocity in rad/s.
#               It is updated in taskEncoder and recorded in taskUser.
#
Duty2 = shares.Share()

##  @brief      The variable, cFlag, is a shared variable
#   @details    This shared variable is a boolean that is shared between 
#               taskUser and taskMotor to determine whether or not the 
#               motor fault should be cleared.
#  
cFlag = shares.Share(False)

if __name__ == '__main__':
    
    # taskList will be the list used to define the three tasks that will run
    # sequentially in 10 ms intervals.
    taskList = [taskEncoder.taskEncoderFcn('taskEncoder', 10_000, zFlag, Data, Delta, Velocity),
                taskUser.taskUserFcn('taskUser', 10_000, zFlag, Data, Delta, Velocity, Duty1, Duty2, cFlag),
                taskMotor.taskMotorFcn('taskMotor', 10_000, Duty1, Duty2, cFlag)]
    
    while True:
        
        # With this loop we want to look for a keyboard interrupt (Ctrl+C).
        # It will try to run the code until Ctrl+C happens and then break.
        try:
            for task in taskList:
                next(task)
            
        except KeyboardInterrupt:
            # A KeyboardInterrupt is ctrl+C in the terminal.
            break
        
    print("Program Terminating")