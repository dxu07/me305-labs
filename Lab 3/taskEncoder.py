'''!
    @file       taskEncoder.py
    
    @brief      The task that interfaces with the encoder driver.
    
    @details    This task's main function is to frequently update the position 
                of the encoder object. It interfaces with the encoder.py driver 
                to update the position. Additionally, this task communicates 
                with the taskUser to determine whether or not the encoder needs 
                to be zeroed.
    
                The source code can be found at my repository under "Lab 3":
                https://bitbucket.org/jakelesher/me-305-labs
                
    @author     Jake Lesher
    @author     Daniel Xu
    @date       02/16/2022
'''

from time import ticks_us, ticks_add, ticks_diff
import pyb  
import encoder, shares 

def taskEncoderFcn(taskName, period, zFlag, Data, Delta, Velocity):
    '''!@brief      This function interacts with the driver to update the 
                    position.
        @details    This function calls upon the driver the update the position 
                    of the encoder frequently so that the position can be 
                    accurately determined. In addition to updating the 
                    position, it also takes the positional and delta data from 
                    the driver. Furthermore, this function also calls upon the 
                    driver to zero the position when needed.
        @param      taskName is the name associated the with the taskEncoder in 
                    main. 
        @param      period is the frequency of which the taskUser is to be run.
        @param      zFlag is the shared variable that communicates the 
                    taskUser to determine whether the encoder should be 
                    zeroed or not.
        @param      Data is the share of positional data in [rad].
        @param      Delta is the share of change in position data in [rad].
        @param      Velocity is the share of velocity data in [rad/s].

    '''
    
    # State 0 is used only for initialization, so it will not exist within 
    # the while loop.
    state = 0

    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    pinPB6 = pyb.Pin(pyb.Pin.cpu.B6, mode=pyb.Pin.IN, af=2)
    pinPB7 = pyb.Pin(pyb.Pin.cpu.B7, mode=pyb.Pin.IN, af=2)
    myEncoder = encoder.Encoder(pinPB6, pinPB7, 4)
    prev_time = start_time
    
    
    # Before starting the while loop, the system will enter state 1 where it
    # will continuously run the encoder driver's update function.
    state = 1
    
    while True:
        current_time = ticks_us()
        if ticks_diff(current_time,next_time)>=0:
            
            # Update 
            if state == 1:
                myEncoder.update()
                
                # Converting position to radians and sharing.
                data_rad = (myEncoder.get_position()/4000)*2*3.14
                Data.write(data_rad)
                
                # Converting delta to rad and sharing.
                delta_rad = (myEncoder.get_delta()/4000)*2*3.14
                Delta.write(delta_rad)
                
                # Calculating velocity from delta and sharing.
                Velocity.write(delta_rad/(ticks_diff(current_time,prev_time)/1000000))
                
                if zFlag.read() == True:
                    state = 2

            # Zero
            elif state == 2: 
                myEncoder.zero()
                zFlag.write(False)
                state = 1
                    
            else: 
                state = 1
                yield state
            prev_time = current_time
            next_time = ticks_add(next_time,period)
        else:
            yield None
            
                
    