'''! 
    @file       DRV8847.py

    @brief      Sets up the motor driver class for the DRV8847 
    @details    This motor driver functions to interact with the two motors 
                connected to the Nucleo board.This motor driver is 
                capable of working with motors connected to pins B4 and B5 or 
                pins B0 and B1. This encoder driver interacts with taskMotor.py 
                initialize, enable, and disable the motors. It also detects if 
                a fault has occured and stops all motors if a fault is detected.
    
                The source code can be found at my repository under "Lab 4":
                https://bitbucket.org/jakelesher/me-305-labs
                
    @author     Jake Lesher
    @author     Daniel Xu
    @date       02/16/2022
    
'''
from motor import Motor
import pyb, utime

class DRV8847:
    '''!    @brief      A motor driver class for the DRV8847 from TI.
            @details    Objects of this class can be used to configure the DRV8847
                        motor driver and to create one or more objects of the
                        Motor class which can be used to perform motor
                        control.
            
                        Refer to the DRV8847 datasheet here:
                        https://www.ti.com/lit/ds/symlink/drv8847.pdf
    
    '''
    def __init__ (self):
        '''!@brief Initializes and returns a DRV8847 object.
        '''
        self.nSleep = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        self.nFAULT = pyb.Pin(pyb.Pin.cpu.B2)
        
        # This code is causing a fault to be triggered immediately.
        #self.MotorInt = pyb.ExtInt(self.nFAULT, mode=pyb.ExtInt.IRQ_FALLING, 
        #                       pull=pyb.Pin.PULL_NONE, callback = self.fault_cb)
        
        self.MotorInt = pyb.ExtInt(self.nFAULT, mode=pyb.ExtInt.IRQ_FALLING, 
                               pull=pyb.Pin.PULL_NONE, callback = self.fault_cb)

        return None
    
    def enable (self):
        '''!@brief Brings the DRV8847 out of sleep mode.
        '''
        self.MotorInt.disable()
        self.nSleep.high()
        utime.sleep_us(25)
        self.MotorInt.enable()
    
    def disable (self):
        '''!@brief Puts the DRV8847 in sleep mode.
        '''
        self.nSleep.low()
        
    
    def fault_cb (self, IRQ_src):
        '''!@brief Callback function to run on fault condition.
        @param IRQ_src The source of the interrupt request.
        '''
        print('Motor fault detected!')
        self.nSleep.low()
        pass
    
    def motor (self, motorNum):
        '''!@brief Creates a DC motor object connected to the DRV8847.
        @return An object of class Motor
        '''
        WM_tim = pyb.Timer(3, freq = 20_000)
        if motorNum == 1:
            IN1_pin = pyb.Pin.cpu.B4
            IN2_pin = pyb.Pin.cpu.B5
        elif motorNum == 2:
            IN1_pin = pyb.Pin.cpu.B0
            IN2_pin = pyb.Pin.cpu.B1
        return Motor(WM_tim, IN1_pin, IN2_pin, motorNum)

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.


    # Create a motor driver object and two motor objects. You will need to
    # modify the code to facilitate passing in the pins and timer objects needed
    # to run the motors.
    motor_drv = DRV8847()
    print("initializing")
    motor_1 = motor_drv.motor(1)
    motor_2 = motor_drv.motor(2)

    # Enable the motor driver
    print("enabling")
    motor_drv.enable()
    
    # Set the duty cycle of the first motor to 40 percent and the duty cycle of
    # the second motor to 60 percent
    print("setting duty")
    motor_1.set_duty(20)
    motor_2.set_duty(20)