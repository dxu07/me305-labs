'''!
    @file       BNO055.py
    
    @brief      Driver to be used with the BNO055 IMU connected to the Nucelo 
                board.
    
    @details    blah blah 
    
                The source code can be found at my repository under "Lab 4":
                https://bitbucket.org/jakelesher/me-305-labs
                
    @author     Jake Lesher
    @author     Daniel Xu
    @date       2/24/2022
    
'''
import pyb, utime
from pyb import I2C

class BNO055:
    '''!@brief      ...
        @details    ...
                    
    '''
    def __init__(self, i2c):
        '''!@brief      
            @details    
            @param      
            @param      
            @return      
            
        '''
        self.i2c = i2c
        self.addr = self.i2c.scan()[0]
        
        self.cal_coef = [0]*22
        self.calbuff = bytearray(22)
        
        self.OPR_MODE = 0x3D # The register for operating mode.
        self.config_mode = 0b00000000 # The byte to send to OPR_MODE for configuation.
        self.NDoF_mode = 0b00001100 # The byte to send to OPR_MODE for NDoF.
        self.i2c.mem_write(self.config_mode, self.addr, self.OPR_MODE)
        utime.sleep_ms(20)
        

        
    def mode(self, modenum):
        '''!@brief      
            @details    
            @param  modenum will be set to 0 for config mode and 1 for NDoF mode.     
            @param      
            @return      
            
        '''
        if modenum == 0:
            self.i2c.mem_write(self.config_mode, self.addr, self.OPR_MODE)
        elif modenum == 1:
            self.i2c.mem_write(self.NDoF_mode, self.addr, self.OPR_MODE)

        
    def status(self):
        '''!@brief      
            @details    
            @param      
            @param      
            @return      
            
        '''
        self.cal_byte = self.i2c.mem_read(1, self.addr, 0x35)[0]
        self.mag_stat = self.cal_byte & 0b00000011
        self.acc_stat = (self.cal_byte & 0b00001100)>>2
        self.gyr_stat = (self.cal_byte & 0b00110000)>>4
        self.sys_stat = (self.cal_byte & 0b11000000)>>6
        return self.mag_stat, self.acc_stat, self.gyr_stat, self.sys_stat

    def read_coef(self):
        '''!@brief      
            @details    
            @param      
            @param      
            @return      
            
        '''
        self.reg_addr = 0x55
        self.cal_coef = self.i2c.mem_read(self.calbuff, self.addr, self.reg_addr)
        
        # for i in range(0, len(self.cal_coef)-1):
        #     self.cal_coef[i] = self.i2c.mem_read(1, self.addr, self.reg_addr)
        #     self.reg_addr -= 0x01
        return self.cal_coef
        
        
    def write_coef(self, cal_coefs):
        '''!@brief      
            @details    
            @param      
            @param      
            @return      
            
        '''
        self.reg_addr = 0x55
        self.i2c.mem_write(cal_coefs, self.addr, self.reg_addr)
        
        # for i in range(0, len(self.cal_coef)-1):
        #     self.i2c.mem_write(hex(self.cal_coef[i]), self.addr, self.reg_addr)
        #     self.reg_addr -= 0x01
    
    
    def read_angle(self):
        '''!@brief      
            @details    
            @param      
            @param      
            @return      
            
        '''
        Euler = self.i2c.mem_read(6, self.addr, 0x1A)
        
        self.Heading = (Euler[1] << 8 | Euler[0])
        if self.Heading > 32767:
            self.Heading -= 65536
        
        self.Roll = (Euler[3] << 8 | Euler [2])
        if self.Roll > 32767:
            self.Roll -= 65536
        
        self.Pitch  = (Euler[5] << 8 | Euler[4])       
        if self.Pitch > 32767:
            self.Pitch -= 65536
        
        return (self.Roll, self.Pitch, self.Heading)
        
        
    def read_omega(self):
        '''!@brief      
            @details    
            @param      
            @param      
            @return      
            
        '''
        w = self.i2c.mem_read(6, self.addr, 0x14)
        
        self.w_y = (w[1] << 8 | w[0])
        if self.w_y > 32767:
            self.w_y -= 65536
        self.w_y *= -1
        
        self.w_x = (w[3] << 8 | w[2])
        if self.w_x > 32767:
            self.w_x -= 65536
        
        self.w_z = (w[5] << 8 | w[4])
        if self.w_z > 32767:
            self.w_z -= 65536
        
        return (self.w_x,self.w_y,self.w_z)

if __name__ == '__main__':
    # Adjust the following code to write a test program.
    
    i2c = I2C(1, I2C.CONTROLLER)
    IMU = BNO055(i2c)
    IMU.mode(1)
    print(f'{IMU.read_coef()}')
    
    # while True:
    #     if IMU.status() == (3,3,3,0):
    #         print(f'The angles are {IMU.read_angle()}.')
    #         #print(f'The omegas are {IMU.read_omega()}.')
    #         utime.sleep(1)
            
    #     else:
    #         print(f'{IMU.status()}')
            
    
