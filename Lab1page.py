'''!@file               Lab1page.py
    @brief              Creates the main documentation page for Lab 1.
    @details            This file serves the sole purpose of generating html
                        documentation for Lab 1.

    @page page1         Lab 1 Documentation/Deliverables

    @section sec_intro1 Introduction
                        For ME 305 Lab 1, we created a program titled 
                        BlinkingLED.py, which blinks an LED on the Nucleo
                        board according to some preset patterns. 
                        The documentation for this program can be found in the
                        'files' tab of this portfolio. 
    
    @section sec_src1   Lab 1 Source Code
                        The source code for Lab 1 is a file called 
                        BlinkingLED.py and it can be found in my repository at:
                        https://bitbucket.org/jakelesher/me-305-labs
    
    @section sec_vid1   Lab 1 Demo Video
                        A video demonstration of the functionality of this 
                        program can be found at this link: 
                        https://vimeo.com/668040642

    @section sec_fsm1   Lab 1 Task and Transition Diagrams
                        Below are the task and transition diagrams for the
                        finite-state machine used in this program.

    @image html https://i.ibb.co/XXT2hdq/lab1diagrams.jpg "LED Blinking State Transition Diagram" width=1200px

    @author             Jake Lesher
    @author             Daniel Xu
    
    @copyright          License Info

    @date               January 19, 2022
'''