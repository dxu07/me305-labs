'''!@file               Lab2page.py
    @brief              Creates the main documentation page for Lab 2.
    @details            This file serves the sole purpose of generating html
                        documentation for Lab 2.

    @page page2         Lab 2 Documentation/Deliverables

    @section sec_intro2 Introduction
                        ME 305 Lab 2 relies on the use of 5 files that work
                        in tandem to read, zero, and take data from one or two 
                        quadrature decoders through a serial connection with
                        the Nucleo board.
                        
    @section sec_files2 Files in Lab 2
                        Considering a file hierarchy consisting of 3 layers, 
                        main.py and shares.py make up the high level layer. 
                        This layer instantiates objects and runs tasks.
                        The middle layer is made up our our two tasks:
                        taskUser.py and taskEncoder.py which run sequentially.
                        The bottom layer is the driver layer which consists 
                        of only the encoder.py encoder driver.
    
    @section sec_src2   Lab 2 Source Code
                        The source code for Lab 2 can be found in my repository 
                        at: https://bitbucket.org/jakelesher/me-305-labs
    
    @section sec_plot2  Lab 2 Encoder Plot
                        Here is a plot of the output from this program after 
                        pressing 'G' in the user interface to gather data for
                        30 seconds.
                        
                        \htmlonly
                        <a href="https://ibb.co/3zrb831"><img src="https://i.ibb.co/HdFyMS7/Encoder-Position-Plot.png" alt="Encoder-Position-Plot" border="0" /></a>
                        \endhtmlonly

    @section sec_fsm2   Lab 2 Task and Transition Diagrams
                        Here are the task and transition diagrams for the
                        finite-state machine used in this program.
                        
                        \htmlonly
                        <a href="https://ibb.co/YXvRGpF"><img src="https://i.ibb.co/JqXBP5w/ME-305-Lab-2-State-and-Transition-Diagrams.jpg" alt="ME-305-Lab-2-State-and-Transition-Diagrams" border="0" /></a>
                        \endhtmlonly


    @author             Jake Lesher
    @author             Daniel Xu
    
    @copyright          License Info

    @date               February 1, 2022
'''