'''!@file               Lab4page.py
    @brief              Creates the main documentation page for Lab 4.
    @details            This file serves the sole purpose of generating html
                        documentation for Lab 4.

    @page page4         Lab 4 Documentation/Deliverables

    @section sec_intro4 Introduction
                        ME 305 Lab 4 relies on the use of 10 files that work
                        in tandem to read, zero, and take data from one or two 
                        quadrature encoders through a serial connection with
                        the Nucleo board. This program can also interface with 
                        1 or 2 motors, setting their duty cycles and recording
                        their position, change in position, and velocity. 
                        
    @section sec_files4 Files in Lab 4
                        Considering a file hierarchy consisting of 3 layers, 
                        main.py and shares.py make up the high level layer. 
                        This layer instantiates objects and runs tasks.
                        The middle layer is made up our our four tasks:
                        taskUser.py, taskEncoder.py, taskController.py and 
                        taskMotor.py which run sequentially, each with a period 
                        of 0.01 [s]. The bottom layer is the driver layer which 
                        consists of the drivers: encoder.py, motor.py, 
                        ClosedLoop.py, and DRV8847.py.
    
    @section sec_src4   Lab 4 Source Code
                        The source code for Lab 4 can be found in my repository 
                        at: https://bitbucket.org/jakelesher/me-305-labs
    
    @section sec_plot4  Lab 4 Step Response Plots
                        Below are the plots of the step responses we recorded 
                        in the user interface by pressing the "R" key. 
                        These responses were used to tune the Kp and Ki values 
                        so that we could obtain a response that resembles the 
                        step input we requested, seen as Vref. Each of the tests
                        used a Vref of 50 [rad/s].
                        
                        \htmlonly
                        <a href="https://ibb.co/kxHdC72"><img src="https://i.ibb.co/47mQrqR/Trial-1.png" alt="Trial-1" border="0"></a>
                        <a href="https://ibb.co/DbLC25t"><img src="https://i.ibb.co/dc72YGr/Trial-2.png" alt="Trial-2" border="0"></a>
                        <a href="https://ibb.co/yff0Kry"><img src="https://i.ibb.co/7YYvBqW/Trial-3.png" alt="Trial-3" border="0"></a>
                        <a href="https://ibb.co/zPsNK0F"><img src="https://i.ibb.co/j3hH71z/Trial-4.png" alt="Trial-4" border="0"></a>
                        <a href="https://ibb.co/hfM2bFM"><img src="https://i.ibb.co/dgckF4c/Trial-5.png" alt="Trial-5" border="0"></a>
                        <a href="https://ibb.co/QPH4jm6"><img src="https://i.ibb.co/By69Ksg/Trial-6.png" alt="Trial-6" border="0"></a>
                        \endhtmlonly
                        
                        
    @section sec_fsm4   Lab 4 Task, State Transition, and Block Diagrams
                        Here are the task and state transition diagrams for the
                        finite-state machine used in this program. Also included
                        is the block diagram for this closed-loop controller.
                        
                        \htmlonly
                        <a href="https://ibb.co/z64MwXW"><img src="https://i.ibb.co/FHDrt5k/Page1.png" alt="Page1" border="0"></a>
                        <a href="https://ibb.co/8cYkCfw"><img src="https://i.ibb.co/Vmw61zG/Page2.png" alt="Page2" border="0"></a>
                        <a href="https://ibb.co/qs0d8wK"><img src="https://i.ibb.co/YkD3SGm/Page3.png" alt="Page3" border="0"></a>
                        <a href="https://ibb.co/6wyPWX3"><img src="https://i.ibb.co/B3fZCnX/Page4.png" alt="Page4" border="0"></a>
                        <a href="https://ibb.co/yFdZ0Bj"><img src="https://i.ibb.co/7tjZvNm/Page5.png" alt="Page5" border="0"></a>
                        <a href="https://ibb.co/r2Mg9Sd"><img src="https://i.ibb.co/Sw02S8f/Page6.png" alt="Page6" border="0"></a>
                        \endhtmlonly


    @author             Jake Lesher
    @author             Daniel Xu
    
    @copyright          License Info

    @date               February 23, 2022
'''