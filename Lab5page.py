'''!@file               Lab5page.py
    @brief              Creates the main documentation page for Lab 5.
    @details            This file serves the sole purpose of generating html
                        documentation for Lab 5.

    @page page5         Lab 5 Documentation/Deliverables

    @section sec_intro5 Introduction
                        ME 305 Lab 5 relies on the use of 9 files that work
                        in tandem to read and take data from the BNO055 IMU on 
                        our balancing platform. The added functionality includes
                        calibration and the ability to read Euler angles and
                        angular velocities. With this data, we were able to
                        balance/stabilize the platform using the inner control
                        loop only (PID controller).
                        
    @section sec_files5 Files in Lab 5
                        Considering a file hierarchy consisting of 3 layers, 
                        main.py and shares.py make up the high level layer. 
                        This layer instantiates objects and runs tasks.
                        The middle layer is made up our our four tasks:
                        taskUser.py, taskIMU.py, taskController.py and 
                        taskMotor.py which run sequentially, each with a period 
                        of 0.01 [s]. The bottom layer is the driver layer which 
                        consists of the drivers: BNO055.py, motor.py, and
                        ClosedLoop.py. Shown in the source code are the additional
                        files: taskEncoder.py, encoder.py, and DRV8847.py. These
                        files are not currently being used in this lab, but they
                        were left in the repository for future use.
    
    @section sec_src5   Lab 5 Source Code
                        The source code for Lab 5 can be found in my repository 
                        at: https://bitbucket.org/jakelesher/me-305-labs
    
    @section sec_deliv5 Lab 5 Deliverables
                        While there are no physical deliverables for lab 5, we
                        were asked to demonstrate the functionality of our IMU 
                        integration in-class. We showed the instructor that our 
                        system can run through calibration steps or read calibration 
                        constants from a text file. We also showed that we can 
                        print the current Euler angles and angular velocities 
                        from our user interface. Lastly, we demonstrated that
                        our platform, equipped with a closed-loop PID controller,
                        can hold itself flat, rejecting small disturbances such
                        as poking the corner of the platform.


    @author             Jake Lesher
    @author             Daniel Xu
    
    @copyright          License Info

    @date               March 3, 2022
'''