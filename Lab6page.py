'''!@file               Lab6page.py
    @brief              Creates the main documentation page for the final project.
    @details            This file serves the sole purpose of generating html
                        documentation for the final ball-balancing project.

    @page page6         Final Project Documentation/Deliverables

    @section sec_intro6 Introduction
                        The ME 305 final project relies on the use of 11 files 
                        that work in tandem to balance a ball on a tilting 
                        platform. These micropython files are flashed onto a 
                        Nucleo STM32 microcontroller, which acts as the controller
                        for this dynamic system. The controller uses information
                        from the BNO055 IMU to keep track of the x and y angles
                        of the platform, while also using a resistive touchpanel
                        to know the x and y position of the ball. With this 
                        information, the controller is able to calculate the 
                        appropriate duty cycles for each motor at any point in
                        time so that that ball stays as close to the center of
                        the platform as possible.
                        
    @section sec_demo6  Demonstration of Ball-Balancing
                        The following video shows a demonstration of our platform
                        in closed-loop control mode as it balances a steel ball.
                        We were able to tune our cascaded controllers through a 
                        long process of guessing and checking. After some time, we 
                        were able to find gain values that can keep the ball balanced
                        for over a minute.

    @htmlonly           <iframe src="https://player.vimeo.com/video/689423266?h=e72dabe2a9&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" width="720" height="405" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="ME 305 Final Project - Ball Balancing"></iframe>
                        <br>
    @endhtmlonly

    @section sec_files6 Files in the Final Project
                        Considering a file hierarchy consisting of 3 layers, 
                        main.py and shares.py make up the high level layer. 
                        This layer instantiates objects and runs tasks.
                        The middle layer is made up our our four tasks:
                        taskUser.py, taskIMU.py, taskPanel.py, taskController.py and 
                        taskMotor.py which run sequentially, each with a period 
                        of 0.01 [s]. The bottom layer is the driver layer which 
                        consists of the drivers: BNO055.py, motor.py, 
                        touchpanel.py, and ClosedLoop.py. Each of these drivers
                        is designed to be reusable, controlling only the most
                        basic functionality of each of the physical components
                        they represent.
    
    @section sec_src6   Final Project Source Code
                        The source code for the final project can be found in 
                        my repository at: https://bitbucket.org/jakelesher/me-305-labs
    
    @section sec_deliv6 Final Lab Deliverables
                        To dive further into the specifics of this final project,
                        the following subpages have been created:

    @subpage diagrams6  : Task and state transition diagrams.

    @subpage userint6   : Details about the user interface and performance.

    @author             Jake Lesher
    @author             Daniel Xu
    
    @copyright          License Info

    @date               March 3, 2022
'''