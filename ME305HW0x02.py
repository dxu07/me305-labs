'''!@file               ME305HW0x02.py
    @brief              Page for HW 2 for ME 305
    @details            This file serves the sole purpose of generating html
                        for HW 2

    @page page3HW         HW 2 Deliverables
    

    @section hw_an      HW 2 Analysis
                        @image html ME305HW2PG1.png
                        @image html ME305HW2PG2.png
                        @image html ME305HW2PG3.png
                        @image html ME305HW2PG4.png

    @author             Daniel Xu
    
    @copyright          License Info

    @date               February 13, 2022
'''