'''!@file               diagrams6page.py
    @brief              A documentation subpage for the final project.
    @details            This file serves the sole purpose showing the relevant 
                        diagrams that illustrate the structure of the final lab 
                        source code.

    @page diagrams6     Final Project Diagrams

    @section sec_taskdia6 Task Diagram for Final Project
                        The following diagram shows how the 5 middle-tier task
                        files interact with each other using shares. Each of the
                        tasks in our program is run at the same frequency of
                        100 Hz.
                        
    @htmlonly           <a href="https://ibb.co/gyrkwW3"><img src="https://i.ibb.co/9npQ24N/Page1.png" alt="Page1" border="0"></a>
    @endhtmlonly
    
    @section sec_statedia6 State Transition Diagrams for Final Project
                        The following state transition diagrams show how each
                        of our 5 task files operates as a finite state machine.

    @htmlonly           <a href="https://ibb.co/X8YQBnS"><img src="https://i.ibb.co/k1m7nLQ/Page2.png" alt="Page2" border="0"></a>
                        <a href="https://ibb.co/86cgKQ6"><img src="https://i.ibb.co/vZY4wMZ/Page3.png" alt="Page3" border="0"></a>
                        <a href="https://ibb.co/26frW8k"><img src="https://i.ibb.co/8BSwNz7/Page4.png" alt="Page4" border="0"></a>
                        <a href="https://ibb.co/1mV5KdL"><img src="https://i.ibb.co/CHdZvMz/Page5.png" alt="Page5" border="0"></a>
                        <a href="https://ibb.co/bQ3ccqY"><img src="https://i.ibb.co/k5yNNkz/Page6.png" alt="Page6" border="0"></a>
    @endhtmlonly

    @author             Jake Lesher
    @author             Daniel Xu
    
    @copyright          License Info

    @date               March 17, 2022
'''