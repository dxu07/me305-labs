'''!@file               mainpage.py
    @brief              Brief doc for mainpage.py
    @details            Detailed doc for mainpage.py 

    @mainpage           Mechatronics

    @section sec_intro  Mechatronics - ME 305
                        This portfolio provides documentation for the Python
                        files created in ME 305 by Jake Lesher and Daniel Xu.

    @section sec_docs   Documentation and Deliverables 
                        Visit https://dxu07.bitbucket.io/pages.html to
                        see the deliverables for each of the lab/HW assignments.\n 
                        Lab 1: https://dxu07.bitbucket.io/page1.html \n 
                        Lab 2: https://dxu07.bitbucket.io/page2.html \n 
                        Lab 3: https://dxu07.bitbucket.io/page3.html \n 
                        Lab 4: https://dxu07.bitbucket.io/page4.html \n 
                        Lab 5: https://dxu07.bitbucket.io/page5.html\n 
                        Final Project: https://dxu07.bitbucket.io/page6.html \n 
                        
    @section sec_code   Source Code
                        All source code for these labs can be found in my 
                        repository at: https://bitbucket.org/dxu07/me305-labs

    @author             Jake Lesher
    @author             Daniel Xu
    
    @copyright          License Info

    @date               March 16, 2022
'''