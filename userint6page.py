'''!@file               userint6page.py
    @brief              A documentation subpage for the final project.
    @details            This file serves the sole purpose showing the relevant 
                        diagrams that illustrate the structure of the final lab 
                        source code.

    @page userint6      Final Project User Interface

    @section sec_introUI6 Introduction to the User Interface
                        The user interface we constructed for this lab is very 
                        simple, but it operates smoothly and allows for multiple
                        types of user input. The image below shows the "platform
                        wizard" help menu that pops up when you first start up
                        the microcontroller.

    @htmlonly           <a href="https://ibb.co/XjsdrRz"><img src="https://i.ibb.co/mbqYVWH/Putty-GUI.png" alt="Putty-GUI" border="0"></a>
    @endhtmlonly
    
    @section sec_UIvid6 Video of UI
                        The following video shows a quick demonstration of our
                        simple user interface from within PuTTY. This video
                        demonstrates the functionality of each of the menu
                        items in our "platform wizard" help menu.

    @htmlonly           <iframe src="https://player.vimeo.com/video/689477813?h=193cfd8feb&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" width="720" height="388" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="ME 305 Final Project - User Interface Tour"></iframe>
    @endhtmlonly
    
    @section sec_datavid6 Video of Data Recording
                        The following video shows that the ball's 
                        position data and the platform's angular orientation
                        are recorded over a 10 second period after the "G" key 
                        is pressed. This data is printed into PuTTY, where we 
                        can copy it over to Excel to plot later on.

    @htmlonly           <iframe src="https://player.vimeo.com/video/689423559?h=4541d24ba6&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" width="720" height="405" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="ME 305 Final Project - Data Recording and UI"></iframe>
    @endhtmlonly

    @section sec_recdata6 Recorded Data
                        After taking the data printed to our serial interface 
                        (PuTTY), we generated the following plots to show the 
                        performance of our balancing platform. The data used in
                        these plots is from the same trial seen in the video
                        shown above. It can be seen that all of the plots 
                        oscillate near or around zero. It can also be seen that
                        the ball's x position is trailing the Y-angle while the 
                        y position trails the x angle.
                        
    @htmlonly           <a href="https://ibb.co/kSvFDYt"><img src="https://i.ibb.co/87tVxFH/Trial-run-plots.png" alt="Trial-run-plots" border="0"></a>
                        <br>
    @endhtmlonly

    @author             Jake Lesher
    @author             Daniel Xu
    
    @copyright          License Info

    @date               March 17, 2022
'''